from st3m import logging
from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import leds
import time

log = logging.Log(__name__, level=logging.INFO)
log.info("import")

class LedsDance(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        # Default pattern is 1
        self.pattern = 1
        self.bundle_path = '/flash/sys/apps/eldraco-ledsdance'

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        # pick pattern
        for i in range(10):
            petal = ins.captouch.petals[i]
            if petal.pressed:
                self.pattern = i

    def draw(self, ctx: Context) -> None:
        # Clean
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.image(
            f"{self.bundle_path}/camp23.png",
            -99,
            -108,
            198,
            216,
        )

        # Do patterns
        if self.pattern == 0:
            # All red
            leds.set_all_rgb(1.0, 0.0, 0.0)
            leds.update()
        if self.pattern == 1:
            # All red
            leds.set_all_rgb(0, 1.0, 0.0)
            leds.update()
        if self.pattern == 2:
            # All red
            leds.set_all_rgb(0.0, 0.0, 1.0)
            leds.update()
        elif self.pattern == 3:
            # All together rainbow
            for red in range(0,10,1):
                red = red / 10
                for green in range(0,10,1):
                    green = green / 10 
                    for blue in range(0,10,1):
                        blue = blue / 10
                        leds.set_all_rgb(red, green, blue)
                        leds.update()
        elif self.pattern == 4:
            sleep_time = 0.1
            red = 204./255
            green = 51./255
            blue = 255./255
            for led_id in range(40):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
            red = 0./255
            green = 102./255
            blue = 255./255
            for led_id in range(40):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
            red = 0./255
            green = 204./255
            blue = 0./255
            for led_id in range(40):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
            red = 216./255
            green = 219./255
            blue = 20./255
            for led_id in range(40):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
        elif self.pattern == 5:
            sleep_time = 0.1
            red = 255./255
            green = 0
            blue = 0
            # 1 petal
            for led_id in range(0,4):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
            # 2 petal
            red = 0
            green = 255./255
            blue = 0
            for led_id in range(5,9):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
            # 3 petal
            red = 0
            green = 0
            blue = 255./255
            for led_id in range(10,14):
                leds.set_rgb(led_id, red, green, blue)
                leds.update()
                time.sleep(sleep_time)
        elif self.pattern == 6:
            pass
        elif self.pattern == 7:
            pass
        elif self.pattern == 8:
            # All white
            leds.set_all_rgb(1.0, 1.0, 1.0)
            leds.update()
        elif self.pattern == 9:
            # All out
            leds.set_all_rgb(0, 0, 0)
            leds.update()

        # Put them out
        #leds.set_all_rgb(0, 0, 0)
        #leds.update()